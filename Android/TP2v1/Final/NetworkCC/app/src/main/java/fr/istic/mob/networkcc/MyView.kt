package fr.istic.mob.networkcc

import android.app.AlertDialog
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView


class MyView(context: Context, attrs: AttributeSet?) : View(context, attrs){
    lateinit var selectedMode : MainActivity.Mode
    private lateinit var layoutInflater: LayoutInflater
    private lateinit var bitmap: Bitmap
    private lateinit var canvas: Canvas
    private lateinit var onFocusRect : NodeObject
    private var deplacementMode :Boolean = false
    private var currentX = 0
    private var currentY = 0
    private lateinit var graph : Graph
    private val paint : Paint = Paint()
    private var pathLine : Path? = null
    private var paintLine: Paint = Paint()
    private var lineCreationList : ArrayList<Path> = arrayListOf()
    private lateinit var currentAlertDialog : AlertDialog.Builder
    private lateinit var  dialogLineWidth : AlertDialog
    private lateinit var widthImageView: ImageView
    private val paintText : Paint = Paint()

    init {
        paint.isAntiAlias = true
        paint.color = Color.BLUE
        paintText.style = Paint.Style.FILL
        paintText.color = Color.BLACK
        paintText.textSize = 30F
        paintLine.isAntiAlias = true
        paintLine.color = Color.BLACK
        paintLine.strokeWidth = 10f
        paintLine.style = Paint.Style.STROKE
        paintLine.strokeCap = Paint.Cap.ROUND
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        canvas = Canvas(bitmap)
        bitmap.eraseColor(Color.WHITE)
    }



    override fun onTouchEvent(event: MotionEvent): Boolean {
        currentX = event.x.toInt()
        currentY= event.y.toInt()
        var rect : Rect = Rect(currentX - 50, currentY - 50, currentX + 50, currentY + 50)

        when(event.action){
            MotionEvent.ACTION_DOWN ->
                if (selectedMode == MainActivity.Mode.OBJECT_CREATION) {
                    if (graph.isThereObject(rect) != null) {
                        deplacementMode = true
                        onFocusRect = graph.isThereObject(rect)!!
                    }
                } else if (selectedMode == MainActivity.Mode.CONNECTION_CREATION) {
                    if (graph.isThereObject(rect) != null) {
                        onFocusRect = graph.isThereObject(rect)!!
                        pathLine = Path()
                        pathLine!!.moveTo(onFocusRect.rect.exactCenterX(), onFocusRect.rect.exactCenterY())
                    }
                }
            MotionEvent.ACTION_MOVE ->
                if (selectedMode == MainActivity.Mode.OBJECT_CREATION && deplacementMode) {
                    onFocusRect.rect.offsetTo(currentX, currentY)
                    onFocusRect.updateLines()
                    invalidate()
                } else if (selectedMode == MainActivity.Mode.CONNECTION_CREATION && pathLine != null) {
                    pathLine!!.reset()
                    pathLine!!.moveTo(onFocusRect.rect.exactCenterX(), onFocusRect.rect.exactCenterY())
                    pathLine!!.lineTo(event.x, event.y)
                    lineCreationList.add(pathLine!!)
                    invalidate()
                }
            MotionEvent.ACTION_UP -> {
                if (selectedMode == MainActivity.Mode.OBJECT_CREATION) {
                    val eventDuration = (event.eventTime - event.downTime)
                    var nodeObject: NodeObject = NodeObject()
                    if (graph.isThereObject(rect) == null && (eventDuration > 700)) {
                        nodeObject.rect = rect;
                        showWidthDialog(nodeObject)
                    }
                    deplacementMode = false
                } else if (selectedMode == MainActivity.Mode.CONNECTION_CREATION) {
                    var objectNodeAux = graph.isThereObject(rect)
                    if (objectNodeAux != null && objectNodeAux != onFocusRect && pathLine!=null) {
                        pathLine!!.reset()
                        pathLine!!.moveTo(onFocusRect.rect.exactCenterX(), onFocusRect.rect.exactCenterY())
                        pathLine!!.lineTo(objectNodeAux.rect.exactCenterX(),objectNodeAux.rect.exactCenterY())
                        objectNodeAux.connectionList[onFocusRect] = pathLine!!
                        onFocusRect.connectionList[objectNodeAux] = pathLine!!
                    }else{
                        pathLine!!.reset()
                    }
                    lineCreationList.clear()
                    invalidate()
                }
            }
        }
        return true
    }
    public fun showWidthDialog(nodeObject : NodeObject?) {
        currentAlertDialog = AlertDialog.Builder(this.context)
        val view: View = layoutInflater.inflate(R.layout.object_dialog, null)
        val setWidthButton: Button = view.findViewById(R.id.widthDialogButton)
        val libelleObjet: TextView = view.findViewById(R.id.libelleObject)
        widthImageView = view.findViewById(R.id.imageViewId)
        setWidthButton.setOnClickListener {
            if (nodeObject != null) { // création par clic
                nodeObject.libelle = libelleObjet.text.toString();
                graph.getListRect().add(nodeObject)
            } else { //création par menu
                var nodeObject: NodeObject = NodeObject()
                nodeObject.libelle = libelleObjet.text.toString();
                var rect = Rect((width/2)-50,(height/2)-50,(width/2)+50,(height/2)+50 )
                nodeObject.rect = rect
                graph.getListRect().add(nodeObject)
            }
            invalidate()
            dialogLineWidth.dismiss()
        }
        currentAlertDialog.setView(view)
        dialogLineWidth = currentAlertDialog.create()
        dialogLineWidth.setTitle("Création d'un objet connecté")
        dialogLineWidth.show()
    }

    override fun onDraw(canvas: Canvas){
        super.onDraw(canvas)
        canvas.drawBitmap(bitmap, 0f, 0f, null)
        for (nodeObject: NodeObject in graph.getListRect()) {
            canvas.drawRect(nodeObject.rect, paint)
            var text = nodeObject.libelle
            var coordText = (text.length * 7).toFloat()
            canvas.drawText(
                text,
                nodeObject.rect.centerX() - coordText,
                nodeObject.rect.centerY() + 80.toFloat(),
                paintText
            )
        }
        for(line : Path in lineCreationList){
            canvas.drawPath(line,paintLine)
        }
        for(line : Path in graph.getListConnection()){
            canvas.drawPath(line, paintLine)
        }
    }
    public fun setGraph(graph: Graph){
        this.graph = graph
    }
    fun setLayoutInflater(layoutInflater: LayoutInflater) {
        this.layoutInflater = layoutInflater
    }



}