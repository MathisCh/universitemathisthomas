package fr.istic.mob.networkcc


import android.graphics.Path
import android.graphics.Rect

class Graph {
   private var listRect = arrayListOf<NodeObject>()
   private var listConnection = arrayListOf<Path>() // à voir pour l'unicité des lignes
    fun isThereObject(rect: Rect): NodeObject?{
        for(rectObject in listRect){
            if((rectObject.rect.left < rect.left &&  rect.left < rectObject.rect.right)
                && (rectObject.rect.top < rect.top && rect.top < rectObject.rect.bottom)){
                return rectObject
            }
            if((rectObject.rect.left < rect.right &&  rect.right < rectObject.rect.right)
                && (rectObject.rect.top < rect.top && rect.top < rectObject.rect.bottom)){
                return rectObject
            }
            if((rectObject.rect.left < rect.right &&  rect.right < rectObject.rect.right)
                && (rectObject.rect.top < rect.bottom && rect.bottom < rectObject.rect.bottom)){
                return rectObject
            }
            if((rectObject.rect.left < rect.left &&  rect.left < rectObject.rect.right)
                && (rectObject.rect.top < rect.bottom && rect.bottom < rectObject.rect.bottom)){
                return rectObject
            }
        }
        return null
    }

    fun getListRect():ArrayList<NodeObject>{
        return listRect
    }

    fun getListConnection():ArrayList<Path>{
        listConnection.clear()
        for (nodeObject in listRect){
            for(node : NodeObject in nodeObject.connectionList.keys){
                nodeObject.connectionList[node]?.let { listConnection.add(it) }
            }
        }
        return listConnection
    }

    fun clear (){
        listRect.clear()
        listConnection.clear()
    }
}