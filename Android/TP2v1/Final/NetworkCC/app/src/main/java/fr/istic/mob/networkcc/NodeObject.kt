package fr.istic.mob.networkcc

import android.graphics.Path
import android.graphics.Rect
class NodeObject {
    lateinit var rect: Rect
    lateinit var libelle: String
    var connectionList : HashMap<NodeObject, Path> = HashMap()

    fun updateLines(){
        for(nodes : NodeObject in connectionList.keys){
            var path : Path? = connectionList[nodes]
            if (path != null){
                path!!.reset()
                path.moveTo(this.rect.exactCenterX(),this.rect.exactCenterY())
                path.lineTo(nodes.rect.exactCenterX(), nodes.rect.exactCenterY())
                nodes.connectionList[this] = path
            }
        }
    }
}