package fr.istic.mob.networkcc

import android.os.Build
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN

class MainActivity : AppCompatActivity() {
    private lateinit var myView: MyView
    var graph : Graph = Graph()
    enum class Mode {
        OBJECT_CREATION, CONNECTION_CREATION, MODIFICATION
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        myView =findViewById(R.id.myView)
        myView.selectedMode = Mode.OBJECT_CREATION
        myView.setLayoutInflater(layoutInflater)
        myView.setGraph(graph)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.RAZ -> {
                graph.clear()
                myView.invalidate()
            }
            R.id.addObject -> {
                myView.selectedMode = Mode.OBJECT_CREATION
                myView.showWidthDialog(null)
            }
            R.id.addConnection -> {
                myView.selectedMode = Mode.CONNECTION_CREATION
            }
            R.id.modify -> {
                myView.selectedMode = Mode.MODIFICATION
            }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }
}